import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { shoeArr } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detailShoe: shoeArr[0],
    cart: [],
  };
  handleChangeDetail = (shoe) => {
    this.setState({ detailShoe: shoe });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    // th1 : sp chưa có trong giỏi hàng => tạo object mới gồn thông tin của object cũ, có thêm key soLuong:1 ( có push )
    // th2 : sp đã có trong giỏ hàng:update value của key soLuong ( không push )
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      // ko tìm thấy => trong giỏ hàng chưa có
      // th1
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      // th2
      // tìm thấy => ko push, update thôi
      cloneCart[index].soLuong++;
    }
    this.setState({ cart: cloneCart });
  };
  handleDelete = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id == id;
    });
    cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  handleChangeQuantity = (id, luaChon) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((shoe) => {
      return shoe.id == id;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
    // ** cloneCart[index].soLuong => true thì chạy splice, false thì dừng
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <div className="row">
          <div className="col-8">
            <CartShoe
              handleChangeQuantity={this.handleChangeQuantity}
              handleDelete={this.handleDelete}
              cart={this.state.cart}
            />
          </div>
          <div className="col-4">
            <ListShoe
              handleViewDetail={this.handleChangeDetail}
              list={this.state.shoeArr}
              handleAddToCart={this.handleAddToCart}
            />
          </div>
        </div>
        <DetailShoe detailShoe={this.state.detailShoe} />
      </div>
    );
  }
}

// truthy falsy

// 6 giá trị falsy: "",0,NaN,false,null,undefined
