import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    return (
      <div className="col-6">
        <div className="card text-left">
          <img
            style={{ width: "10vw" }}
            className="card-img-top"
            src={this.props.dataShoe.image}
          />
          <div className="card-body text-center">
            <h4 className="card-title">{this.props.dataShoe.price}</h4>
            <button
              onClick={() => {
                this.props.handleOnclickDetail(this.props.dataShoe);
              }}
              className="btn btn-warning"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.dataShoe);
              }}
              className="btn btn-success"
            >
              Add
            </button>
          </div>
        </div>
      </div>
    );
  }
}
/**
 * {
  "dataShoe,": {
    "id,": 1,
    "name,": "Adidas Prophere",
    "alias,": "adidas-prophere",
    "price,": 350,
    "description,": "The adidas Primeknit upper wraps the foot with a supportive fit that enhances movement.\r\n\r\n",
    "shortDescription,": "The midsole contains 20% more Boost for an amplified Boost feeling.\r\n\r\n",
    "quantity,": 995,
    "image,": "http://svcy3.myclass.vn/images/adidas-prophere.png"
  }
}
 */
