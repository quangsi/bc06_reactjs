import React, { Component } from "react";

export default class EventHandling extends Component {
  // state : quản lý các data liên quan đến layout
  state = {
    username: "alice",
  };
  //   dùng setState() để update state, sau khi setState() thì render() sẽ được chạy lại => render ra layout mới
  handleChangeUsername = () => {
    let value = this.state.username == "Bob" ? "Alice" : "Bob";
    // this.username = "bob";
    this.setState({
      username: value,
    });
  };

  render() {
    return (
      <div>
        <h2>EventHandling</h2>
        <h4
          className={
            this.state.username == "Bob" ? "text-danger" : "text-success"
          }
        >
          Username: {this.state.username}
        </h4>
        <button onClick={this.handleChangeUsername} className="btn btn-success">
          Change username
        </button>
      </div>
    );
  }
}
