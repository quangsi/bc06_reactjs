import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import DataBinding from "./DataBinding/DataBinding";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import EventHandling from "./EventHandling/EventHandling";
import Ex_Number from "./Ex_Number/Ex_Number";
import Ex_Car from "./Ex_Car/Ex_Car";
import RenderWithMap from "./RenderWithMap/RenderWithMap";
import DemoProps from "./DemoProps/DemoProps";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import DemoReduxMini from "./DemoReduxMini/DemoReduxMini";
import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";

function App() {
  return (
    <div className="App">
      {/* --- part 1 --- */}
      {/* <DemoClass /> */}
      {/* <DemoClass></DemoClass> */}
      {/* <DemoFunction /> */}
      {/* <DataBinding /> */}
      {/* <Ex_Layout /> */}
      {/* --- part 2 --- */}
      {/* <EventHandling /> */}
      {/* <Ex_Number /> */}
      {/* <Ex_Car /> */}
      {/* <RenderWithMap /> */}
      {/* --- part 3 (props) --- */}
      {/* <DemoProps /> */}
      {/* <Ex_ShoeShop /> */}
      {/* --- Redux --- */}
      {/* <DemoReduxMini /> */}
      <Ex_ShoeShop_Redux />
    </div>
  );
}

export default App;
