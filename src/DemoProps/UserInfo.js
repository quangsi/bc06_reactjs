import React, { Component } from "react";

export default class UserInfo extends Component {
  render() {
    console.log(this.props);
    let { age, name } = this.props;
    return (
      <div className="p-5 bg-warning">
        <h2>Username: {name}</h2>
        <h3>Age: {age}</h3>
        <button onClick={this.props.handleOnclick} className="btn btn-danger">
          Change username
        </button>
      </div>
    );
  }
}
