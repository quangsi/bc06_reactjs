import React, { Component } from "react";
import UserInfo from "./UserInfo";

export default class DemoProps extends Component {
  state = {
    username: "Alice",
    age: 2,
  };
  //   state ở đâu thì viết setState tại đó
  handleChangeUsername = () => {
    this.setState({
      username: "Bob" + Math.random(),
    });
  };

  render() {
    return (
      <div className="p-5 bg-success">
        <UserInfo
          name={this.state.username}
          age={this.state.age}
          handleOnclick={this.handleChangeUsername}
        />
      </div>
    );
  }
}
