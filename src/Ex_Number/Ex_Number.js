import React, { Component } from "react";

export default class Ex_Number extends Component {
  state = {
    number: 100,
  };
  handleChangeNumber = (soLuong) => {
    this.setState({
      number: this.state.number + soLuong,
    });
  };

  render() {
    return (
      <div>
        <h2>Ex_Number</h2>
        <button
          onClick={() => {
            this.handleChangeNumber(-1);
          }}
          className="btn btn-danger"
        >
          -
        </button>
        <strong>{this.state.number}</strong>
        <button
          onClick={() => {
            this.handleChangeNumber(1);
          }}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}
