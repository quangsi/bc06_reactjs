import React, { Component } from "react";

export default class Ex_Car extends Component {
  state = {
    imgSrc: "./img/CarBasic/products/red-car.jpg",
  };
  handleChangeColor = (color) => {
    let newImg = `./img/CarBasic/products/${color}-car.jpg`;
    this.setState({ imgSrc: newImg });
  };

  render() {
    return (
      <div className="container row">
        <img className="col-6" src={this.state.imgSrc} alt="" />
        <div className="col-6 pt-5">
          <button
            onClick={() => {
              this.handleChangeColor("red");
            }}
            className="btn btn-danger"
          >
            Red
          </button>
          <button
            onClick={() => {
              this.handleChangeColor("black");
            }}
            className="btn btn-dark mx-5"
          >
            Black
          </button>
          <button
            onClick={() => {
              this.handleChangeColor("silver");
            }}
            className="btn btn-secondary"
          >
            Gray
          </button>
        </div>
      </div>
    );
  }
}
