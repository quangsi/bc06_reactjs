import React, { Component } from "react";

export default class DataBinding extends Component {
  imgSrc =
    "https://bizweb.dktcdn.net/100/442/328/files/bun-bo-hue-dac-san-dan-da-lam-say-long-biet-bao-thuc-khach-4.jpg?v=1638937219225";

  render() {
    let title = "Bún bò Huệ";
    return (
      <div className="text-danger">
        <div className="card" style={{ width: "18rem", color: "blue" }}>
          <img src={this.imgSrc} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{title}</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
      </div>
    );
  }
}

// rcc
