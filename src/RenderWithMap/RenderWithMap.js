import React, { Component } from "react";
import { movieArr } from "./dataMovie";

export default class RenderWithMap extends Component {
  state = {
    movieDetail: movieArr[0],
  };
  handleChangeDetail = (movie) => {
    this.setState({ movieDetail: movie });
  };

  renderMovieList = () => {
    let movieList = movieArr.map((item) => {
      return (
        <div className="card col-2" style={{ width: "18rem" }}>
          <img
            style={{ height: "200px", objectFit: "cover" }}
            src={item.hinhAnh}
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">
              {/* {item.tenPhim.length > 10
                ? item.tenPhim.slice(0, 10) + "..."
                : item.tenPhim} */}
              {item.tenPhim}
            </h5>

            <button
              onClick={() => {
                this.handleChangeDetail(item);
              }}
              className="btn btn-primary"
            >
              Xem ngay
            </button>
          </div>
        </div>
      );
    });
    return movieList;
  };

  render() {
    return (
      <div>
        <div className="display-4 text-danger">
          Name: {this.state.movieDetail.tenPhim}
        </div>
        <div className="row">{this.renderMovieList()}</div>
      </div>
    );
  }
}

// card bs4
