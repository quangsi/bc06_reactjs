import React, { Component } from "react";
import { connect } from "react-redux";

class DemoReduxMini extends Component {
  render() {
    console.log("props", this.props);
    return (
      <div>
        <button
          onClick={() => {
            this.props.handleGiam(10);
          }}
          className="btn btn-danger"
        >
          -
        </button>
        <strong className="mx-5">{this.props.currentNumber}</strong>
        <button onClick={this.props.handleTang} className="btn btn-success">
          +
        </button>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    currentNumber: state.soLuong.number,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTang: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiam: (number = 1) => {
      let action = {
        type: "GIAM_SO_LUONG",
        payload: number,
      };
      dispatch(action);
    },
  };
};

// key => props
// value: giá trị trên state
export default connect(mapStateToProps, mapDispatchToProps)(DemoReduxMini);
